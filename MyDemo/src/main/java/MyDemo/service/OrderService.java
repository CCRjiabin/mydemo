package MyDemo.service;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import MyDemo.beans.Order;
import MyDemo.dao.OrderDao;
import MyDemo.http.Response;

@Service
@Transactional
public class OrderService {
	@Autowired
	OrderDao orderDao;
	
	public Response addOrder(Order order) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		long num = timestamp.getTime();
		String ordernum = Long.toString(num);
		order.setOrderNum(ordernum);
		orderDao.save(order);
		return new Response(true);
	}
}
