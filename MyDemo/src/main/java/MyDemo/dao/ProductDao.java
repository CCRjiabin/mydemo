package MyDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import MyDemo.beans.Product;

public interface ProductDao extends JpaRepository<Product, Integer> {
	
}
