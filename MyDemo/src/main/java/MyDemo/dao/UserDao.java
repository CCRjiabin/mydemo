package MyDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import MyDemo.beans.User;

public interface UserDao extends JpaRepository<User, Integer> {
	User findByUsername(String username);
}
