package MyDemo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import MyDemo.beans.Buy_pro;

public interface BuyDao extends JpaRepository<Buy_pro, Integer>{
	List<Buy_pro> findByorders_id(int id);
	
	void deleteByorders_id(int id);
}
