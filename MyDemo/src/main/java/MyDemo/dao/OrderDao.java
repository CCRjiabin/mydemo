package MyDemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import MyDemo.beans.Order;

public interface OrderDao extends JpaRepository<Order, Integer> {
	void deleteByid(int id);
}
