package MyDemo.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
	@SequenceGenerator(name = "SEQ", sequenceName = "PRO_SEQ")
	private int id;
	
	@Column(name = "pro_name", unique = true, nullable = false)
	private String pro_name;
	
	@Column(name = "price", nullable = false)
	private double price;
	
	@Column(name = "pic")
	private String pic;

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(int id, String pro_name, double price, String pic) {
		super();
		this.id = id;
		this.pro_name = pro_name;
		this.price = price;
		this.pic = pic;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPro_name() {
		return pro_name;
	}

	public void setPro_name(String pro_name) {
		this.pro_name = pro_name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", pro_name=" + pro_name + ", price=" + price + ", pic=" + pic + "]";
	}
	

}
