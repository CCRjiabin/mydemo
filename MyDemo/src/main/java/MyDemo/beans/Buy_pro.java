package MyDemo.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "BUY_PRO")
public class Buy_pro {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ")
	@SequenceGenerator(name = "SEQ", sequenceName = "BUYPRO_SEQ")
	private int buy_pro_id;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orders_id")
	private Order orders;
	
	@Column
	private int pro_id;
	
	@Column
	private int cnt;
	
	public int getId() {
		return buy_pro_id;
	}

	public void setId(int id) {
		this.buy_pro_id = id;
	}

	public int getPro_id() {
		return pro_id;
	}

	public void setPro_id(int pro_id) {
		this.pro_id = pro_id;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public Buy_pro() {
		super();
	}

	public int getBuy_pro_id() {
		return buy_pro_id;
	}

	public void setBuy_pro_id(int buy_pro_id) {
		this.buy_pro_id = buy_pro_id;
	}

	public Order getOrders() {
		return orders;
	}

	public void setOrders(Order orders) {
		this.orders = orders;
	}

	public Buy_pro(int buy_pro_id, Order orders, int pro_id, int cnt) {
		super();
		this.buy_pro_id = buy_pro_id;
		this.orders = orders;
		this.pro_id = pro_id;
		this.cnt = cnt;
	}

	@Override
	public String toString() {
		return "Buy_pro [buy_pro_id=" + buy_pro_id + ", orders=" + orders + ", pro_id=" + pro_id + ", cnt=" + cnt + "]";
	}
}
