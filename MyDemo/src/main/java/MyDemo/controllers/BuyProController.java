package MyDemo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import MyDemo.beans.Buy_pro;
import MyDemo.dao.BuyDao;
import MyDemo.http.Response;

@RestController()
public class BuyProController {
	@Autowired
	BuyDao buyProDao;
	
	@GetMapping("/buypros/{orderNum}")
	public List<Buy_pro> getAllPros(@PathVariable int orderNum) {
		return buyProDao.findByorders_id(orderNum);
	}
	
	@GetMapping("/buypros")
	public List<Buy_pro> getAllPros() {
		return buyProDao.findAll();
	}
	
	@Transactional
	@DeleteMapping("/buycabs/{orderNum}")
	public Response deleteThePros(@PathVariable int orderNum) {
		buyProDao.deleteByorders_id(orderNum);
		return new Response(true);
	}
}
