package MyDemo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import MyDemo.beans.Order;
import MyDemo.dao.BuyDao;
import MyDemo.dao.OrderDao;
import MyDemo.http.Response;
import MyDemo.service.OrderService;

@RestController()
public class OrderController {
	@Autowired
	OrderService orderService;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	BuyDao buyProDao;
	
	@PostMapping("/order")
	public Response addOrder(@RequestBody Order order) {
		order.getPro().forEach(pro -> pro.setOrders(order));
		return orderService.addOrder(order);
	}
	
	@GetMapping("/order")
	public List<Order> getAllOrders() {
		return orderDao.findAll();
	}
	
	@Transactional
	@DeleteMapping("/order/{orderNum}")
	public Response deleteTheOrder(@PathVariable int orderNum) {
		buyProDao.deleteByorders_id(orderNum);
		orderDao.deleteByid(orderNum);
		return new Response(true);
	}
}
