package MyDemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import MyDemo.beans.User;
import MyDemo.http.Response;
import MyDemo.service.UserService;


@RestController()
public class AdminController {
	@Autowired
	UserService userService;
	
	@PostMapping("/admin")
	public Response addUser(@RequestBody User user) {
		return userService.registerAdm(user);
	}
}
