package MyDemo.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import MyDemo.service.ProductService;
import MyDemo.beans.Product;
import MyDemo.dao.ProductDao;
import MyDemo.http.Response;

@RestController()
public class ProductController {
	@Autowired
	ProductService productService;
	
	@Autowired
	ProductDao productDao;
	
	public List<String> files = new ArrayList<String>();
	
	@GetMapping("/products")
	public List<Product> getProductsList() {
		return productService.getProductsList();
	}
	
	@GetMapping("/products/{id}")
	public Product getOnePro(@PathVariable int id) {
		return productDao.findOne(id);
	}
	
	@PostMapping("/products")
	public Response addProduct(@ModelAttribute Product product, @RequestParam("product_image") MultipartFile file) {
		product.setPic(file.getOriginalFilename());
		try {
			productService.store(file);
			files.add(file.getOriginalFilename());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return productService.addProduct(product);
	}
	
	@GetMapping("/getallfiles")
	public ResponseEntity<List<String>> getListFiles(Model model) {
		
		List<String> results = new ArrayList<String>();
		File[] pics = new File("products").listFiles();
		for(File pic : pics) {
			results.add(pic.getName());
		}
		
		List<String> fileNames = results
				.stream().map(fileName -> MvcUriComponentsBuilder
						.fromMethodName(ProductController.class, "getFile", fileName).build().toString())
				.collect(Collectors.toList());

		return ResponseEntity.ok().body(fileNames);
	}
		
	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile (@PathVariable String filename) {
		Resource file = productService.loadFile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
		
		//method to get one pic
	@GetMapping("/pic/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getPic(@PathVariable String filename) {
		Resource file = productService.loadFile(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}
	
}
